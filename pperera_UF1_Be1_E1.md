**Laboratori: M3 - UF1 - T1 - A1 - Be1 - E1**	

UF1. Programació estructurada.	


|Material de consulta:|<p>**Teoria:**</p><p>[UF1 - T1 - Estructura d'un programa informàtic](http://portal.almata.es/moodle/mod/resource/view.php?id=9864).</p><p>**Lectures recomanades:**</p><p>“*2 Tipus Simples en C. El printf*” [“*3 Constants en C*.](http://portal.almata.es/moodle/mod/resource/view.php?id=10566)”</p><p>**Explicacions de classe.**</p>|
| :- | :- |
|Format del lliurament:|<p>**nomUsuari\_UF1\_Be1\_E1**</p><p>Per exemple si l’usuari és *asanchez* el nom seria “*asanchez \_UF1\_Be1\_E1*”</p><p>Les respostes feu-les en color blau.</p>|
|Durada:|Establerta al portal web de l’assignatura.|

# Estructura d’un programa informàtic.
## 1.  Projectes de desenvolupament d’aplicacions. Entorn integrats de desenvolupament.

### Exercici 1:	 Cerca al mercat actual un IDE propietari i un de codi lliure on es pugui programar en ANSI C. Comenta, a grans trets, les característiques de cada un d’ells.Creus que el *CodeBlocks* és un bon IDE per programar en C? Raona la resposta.

<span style="color:blue">

**IDE lliure:**

**Netbeans** és un IDE multiplataforma gratuït, de codi obert i popular per a C/C++ i molts altres llenguatges de programació. És completament extensible utilitzant complements desenvolupats per la comunitat.

IDE propietari:

**FlashBuilder**: (anteriorment Adobe Flex Builder ) és un entorn de desenvolupament integrat, escrit a la plataforma Eclipse destinat per al desenvolupament d'aplicacions d'Internet enriquides (RIA) i aplicacions d'escriptori multiplataforma , particularment per a la plataforma d'Adobe Flash . El suport per a aplicacions d'escriptori multiplataforma va ser afegit a Flex Builder 3 amb la introducció d'Adobe AIR .

Crec que el **CodeBlocks** és un bon IDE per a programar en C perquè és multiplataforma gratuït, altament extensible i configurable, creat per oferir als usuaris les característiques més demandades i ideals.
</span>




## 2. Blocs d’un programa informàtic.

### Exercici 2:	 Mostra els blocs de codi d’un programa en ANSI C. Comenta’ls breument. Quins blocs apareixeran com a mínim en tot programa C?
<span style="color:blue">


     #include <stdio.h>

     #include <stdlib.h>

     int main(){

          printf("Hola mon");

     return 0;

     }

Apareixen les llibreries stdio.h i stdlib.h sempre alinciar un nou projecte al codeblocks.

El bloc principal es el int main(){} i return 0 per a sortir del bloc i acabar el codi principal.
</span>


## 3. Comentaris al codi.

### Exercici 3:	 Explica els tipus de comentaris existents en C i fes un algorisme que exemplifiqui el  seu ús.
<span style="color:blue">
Comentari simple de una línia s’inicia el comentari amb un a doble barra diagonal //.

Comentari de més d’una línia comença amb una barra i un asterisc __/*\**__  *llavors ficaries el comentari de més d’una línia i es tancaria el comentari  amb un asterisc més una barra* __\*/__

     #include <stdio.h>

     #include <stdlib.h>

     int main(){

          //comentari simple d’una linia

          /*comentari

          de

          més

          d’una

          linia */

          return 0;

     }
<span>

## 4. Variables. Tipus i utilitat.

### Exercici 4: Quins són els tipus de dades simples en C? Feu un programa que escrigui a pantalla un valor per cada un dels tipus simples. En els tipus numèrics no heu d’incloure els modificadors *short*, *long, signed i unsigned*. Explica i posa un exemple de desbordament d’una variable de tipus enter? Afegiu una captura de la sortida a pantalla.

#### Nota: Per fer aquest exercici us cal llegir el document “***2 Tipus Simples en C. El printf***” del portal el qual explicat tot el que us cal saber de llenguatge C per fer l’exercici. A més, en aquest mateix document hi teniu exemples.

<span style="color:blue">
Els tipus simples en C són:

Enter int

Caràcter char

Double

Float

Exemple:

     #include <stdio.h>

     #include <stdlib.h>

      int main()

     {

          int a = 1;

          double b = 2.1415;

          float c = 12.25;

          char d='a';

          printf("%i  enter\n",a);

          printf("%f double\n",b);

          printf("%f float\n",c);

          printf("%c caracter\n",d);

          return 0;

     }

![](Aspose.Words.47d9577b-9127-4c07-9bfc-976a2cf4e681.001.png)



Un desbordament  d’una variable de tipus enter succeeix quan el numero enter és el màxim que permet el ordenador i se li suma un numero més, per exemple:

     #include <stdio.h>

     #include <stdlib.h>

     #include <limits.h>

     int main()

     {

          printf("%i",INT\_MAX+1);

          return 0;

     }

![](Aspose.Words.47d9577b-9127-4c07-9bfc-976a2cf4e681.002.png)

</span>
## 5. Formats de sortida. Instrucció *printf*.

### Exercici 5: Feu un programa amb la següent sortida a pantalla:
1. El número 77 en octal
2. El número 65535 en hexadecimal i en majúscules.
3. El número 32727 en hexadecimal i en minúscules.
4. El número 7325 amb el signe.
5. El número 6754 amb deu dígits i farciment de 0s per l’esquerra.
6. El número 456.54378 amb 3 decimals.
7. El teu nom i cognoms.
8. Els primers 8 caràcters de la sortida del punt anterior.

### Afegiu una captura de la sortida a pantalla.

#### Nota: Per fer aquest exercici us cal llegir el document “***2 Tipus Simples en C. El printf***” i més concretament l’apartat de formats de sortida.
<span style="color:blue">

     #include <stdio.h>

     #include <stdlib.h>

     #include <limits.h>

     #define MAX\_TEXT 100

     int main()

     {

          int a=77;

          int b=65535;

          int c=32727;

          int d=7325;

          int e=6754;

          float f=456.54378;

          char nomCongoms[MAX\_TEXT+1]="Pau Nikita Perera Rocaspana";

          printf("%o\n",a);

          printf("%X\n",b);

          printf("%x\n",c);

          printf("+%u\n",d);

          printf("%.10i\n",e);

          printf("%.3f\n",f);

          printf("%s\n",nomCongoms);

          printf("%.8s\n",nomCongoms);

          return 0;

     }

![](Aspose.Words.47d9577b-9127-4c07-9bfc-976a2cf4e681.003.png)

</span>
## 6. Constants. Tipus i utilització.

### Exercici 6: Explica que són les constants i per a que serveixen. Feu un programa que exemplifiqui l’ús de constants.
<span style="color:blue">
Es diu constant a una variable que el seu contingut sol s'escriurà un cop al llarg del programa i no es podrà modificar més.

Per exemple:

     #include <stdio.h>

     #include <stdlib.h>

     int main()

     {

          const float NUMERO\_PI=3.1415;

          printf("%.4f",NUMERO\_PI);

          return 0;

     }
</span>
## 7. Utilització de variables

### Exercici 7:	 Explica que són les variables i per a que serveixen. Relaciona la memòria de la computadora per explicar-ho. Posa’n exemples. Quan utilitzem les variables? Quina relació hi ha entre una variable i la seva adreça de memòria?

<span style="color:blue">
Les variables es el nom de l’etiqueta que utilitzarem per a referir-nos a una cela de memòria, són molt practiques perquè ens ajuden a recordar per un nom en comtes d’una adreça de memòria a on hem de guardar la informació.

Per exemple estem fent un programa on volem guardar la paraula «hola» Si la cela te l’adreça 1234678908542 per exemple i volem guardar un saludo li fiquem el nom salutacions a la cela de aquesta forma char salutacions[MAX\_TEXT+1]=«hola» d’aquesta forma si volem canviar hola per bon dia  sol caldrà referir-nos a salutacions en comtes del numero llarg fent el següent salutacions= «bon dia».
</span>
## 8. Operadors del llenguatge de programació.

### Exercici 8: Cita en forma de taula els operadors relacionals, els aritmètics i els lògics que s’utilitzen en ANSI C. Què és la preferència dels operadors? Posa’n exemples.

<span style="color:blue">

Lògics:
<p> I &&</p>
<p> o ||</p>
<p> no !</p>
<p></p>
 Aritmètics:
<p> Suma + </p>
<p> Resta - </p>
<p> Multiplicació *</p>
<p> divisió div o /</p>
<p> Residu de la divisió mod</p>
<p></p>
Relacionals:
<p> Igual ==</p>
<p> Estrictament més petit < </p>
<p> Més petit o igual =< </p>
<p> Estrictament major > </p>
<p> Més gran o igual 0=> </p>
<p> Diferent !=< </p>


La preferència dels operadors serveix per quan hi ha més d’un operador en quin ordre es te que resoldre la operació van de esquerra adreta u amb el següents nivells de prioritat.

1. Operador de negació no. !
2. Operadors multiplicadors i divisius. \*, /, div,  mod
3. operadors additius o de resta. +, -
4. Operadors relacionals. =, <, =<, >, => !=
5. Operador de conjunció.  &&
6. operador de disjunció. ||

Exemple: 6\*3/3+2-1
</span>
